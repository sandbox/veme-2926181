Country Field
=============

A Drupal 8 field for selecting a country.

It's primary use is for entityform module forms, as a lightweight alternative to Address module.
